/*---------------------------------------------------------------------------*/
/* 2016-4-16  encode512.c                                                    */
/* a.out                                                                     */
/* mandrill.raw                                                              */
/* codebook512                                                               */
/* mandrill.vq                                                               */
/* Results for  mandrill.vq tiffany.vq scene.vq lenna.vq peppers.vq jet.vq   */
/* PSNR  value  25.37       31.32      29.05    31.74    29.71      30.00    */
/*---------------------------------------------------------------------------*/
#include <stdio.h>
#include <math.h>
#define codebooksize 512
main()
{
 int index[128][128];
 unsigned char temp[512][512];
 unsigned char codebook[codebooksize][4][4];
 char str[20];
 FILE *fp;
 int i,j,m,n,x, kindex;
 unsigned int k;
 float error,dist,MSE,PSNR;
 int count[codebooksize];
/*---------------------------------------------------------------------------*/
/* File to be encoded, for example, lenna.raw                                */
/*---------------------------------------------------------------------------*/
  printf("Input the file to encode (size 512*512)...\n");
  scanf("%s",str);
  fp= fopen(str,"r");
  for(i=0;i<512;i++)
     for(j=0;j<512;j++)
         temp[i][j]=getc(fp);
  fclose(fp);
/*---------------------------------------------------------------------------*/
/* Codebook to be used for encoding, for example, codebook512                */
/*---------------------------------------------------------------------------*/
  printf("Input the codebook...(size 512 of 4*4)...\n");
  scanf("%s",str);
  fp= fopen(str,"r");
  for(i=0;i<codebooksize;i++) { 
     count[i]=0;
     for(j=0;j<4;j++)
       for(k=0;k<4;k++) 
         codebook[i][j][k]=getc(fp);         
  }        
  fclose(fp);
  
/*---------------------------------------------------------------------------*/
/* Main body of encoding                                                     */
/*---------------------------------------------------------------------------*/
  printf("Output the target file [hint: lenna.vq] .....\n");
  scanf("%s",str);
  fp= fopen(str,"w");
  MSE=0;
  PSNR=0;
  for(i=0; i<128; i++)
     for(j=0; j<128; j++) {
       error=2e30;
       for(k=0;k<codebooksize;k++) {
           dist=0;
           for(m=0;m<4;m++)
             for(n=0;n<4;n++)
           { x=abs(temp[4*i+m][4*j+n]-codebook[k][m][n]);         
             dist=dist+x*x;                   
           } /* end for m,n */       
           if(dist<=error)
           { error=dist;
             kindex=k; 
           } /* end if */                 
       } /* end of for k */
/* putc(bk,fp); **if codebook size is no more than 256: a 8-bit representation**
*/
       index[i][j]=kindex;
       count[kindex]++;
       MSE=MSE+error;                      
  } /* end of for i,j */       

/*---------------------------------------------------------------------------*/
/* Output the VQ coding index file (for a 512x512 image file)                */
/*---------------------------------------------------------------------------*/
  for (i=0; i<128; i++)
    for (j=0; j<128; j++)
      fprintf(fp,"%4d",index[i][j]);
  fclose(fp);

/*---------------------------------------------------------------------------*/
/* Extra work: Evaluation for VQ and its reconstruction for a 512x512 image  */
/*            and/or the distribution for each codeword (index distribution) */
/*---------------------------------------------------------------------------*/
  MSE=MSE/(512*512);
  PSNR=10*log10(255*255/MSE);

  for(i=0; i<512; i+=4) {
    for(k=0;k<4;k++) printf("count[%d]=%d;",i+k,count[i+k]);
    printf("\n");
  } 
  for (j=0; j<16; j++) printf("%4d",index[0][j]);
  printf("\n");

  printf("The PSNR value of this image is...%6.2f\n",PSNR);

} /* end of main */
  
