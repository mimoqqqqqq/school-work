%train by 3 images in figure1
clc;clear;close all;
m=512; n=512;
mc=256; nc=256;
fin=fopen('lenna.raw','r');
X=fread(fin,m*n,'uint8=>uint8'); fclose(fin);
Y=reshape(X,m,n); Z1=Y';
subplot(3,3,1)
imshow(Z1)
title("lenna")

fin=fopen('lenna_256.raw','r');
X=fread(fin,m*n,'uint8=>uint8'); fclose(fin);
Y=reshape(X,m,n); Z2=Y';
subplot(3,3,2)
imshow(Z2)
title("CB 256 & PSNR = 30.72")

fin=fopen('lennaR.raw','r');
X=fread(fin,m*n,'uint8=>uint8'); fclose(fin);
Y=reshape(X,m,n); Z2=Y';
subplot(3,3,3)
imshow(Z2)
title("CB 512 & PSNR = 31.67")

%%%%
clc;clear;
m=512; n=512;
fin=fopen('mandrill.raw','r');
X=fread(fin,m*n,'uint8=>uint8'); fclose(fin);
Y=reshape(X,m,n); Z3=Y';
subplot(3,3,4)
imshow(Z3)
title("mandrill")

fin=fopen('mandrill_256.raw','r');
X=fread(fin,m*n,'uint8=>uint8'); fclose(fin);
Y=reshape(X,m,n); Z4=Y';
subplot(3,3,5)
imshow(Z4)
title("CB 256 & PSNR = 24.56")

fin=fopen('mandrillR.raw','r');
X=fread(fin,m*n,'uint8=>uint8'); fclose(fin);
Y=reshape(X,m,n); Z4=Y';
subplot(3,3,6)
imshow(Z4)
title("CB 512 & PSNR = 25.31")

%%%%
fin=fopen('scene.raw','r');
X=fread(fin,m*n,'uint8=>uint8'); fclose(fin);
Y=reshape(X,m,n); Z5=Y';
subplot(3,3,7)
imshow(Z5)
title("scene")

fin=fopen('scene_256.raw','r');
X=fread(fin,m*n,'uint8=>uint8'); fclose(fin);
Y=reshape(X,m,n); Z6=Y';
subplot(3,3,8)
imshow(Z6)
title(" CB 256 & PSNR = 28.19 ")

fin=fopen('sceneR.raw','r');
X=fread(fin,m*n,'uint8=>uint8'); fclose(fin);
Y=reshape(X,m,n); Z6=Y';
subplot(3,3,9)
imshow(Z6)
title(" CB 512 & PSNR = 28.86 ")
%%%%%%**************************************************
figure
clc;clear;
m=512; n=512;

fin=fopen('D23.raw','r');
X=fread(fin,m*n,'uint8=>uint8'); fclose(fin);
Y=reshape(X,m,n); Z1=Y';
subplot(3,3,1)
imshow(Z1)
title("D23")

fin=fopen('D23_256.raw','r');
X=fread(fin,m*n,'uint8=>uint8'); fclose(fin);
Y=reshape(X,m,n); Z2=Y';
subplot(3,3,2)
imshow(Z2)
title("CB 256 & PSNR = 23.78")

fin=fopen('D23R.raw','r');
X=fread(fin,m*n,'uint8=>uint8'); fclose(fin);
Y=reshape(X,m,n); Z2=Y';
subplot(3,3,3)
imshow(Z2)
title("CB 512 & PSNR = 24.34")
%%%%%%%%%%%
fin=fopen('fingerprint.raw','r');
X=fread(fin,m*n,'uint8=>uint8'); fclose(fin);
Y=reshape(X,m,n); Z3=Y';
subplot(3,3,4)
imshow(Z3)
title("fingerprint")

fin=fopen('fingerprint_256.raw','r');
X=fread(fin,m*n,'uint8=>uint8'); fclose(fin);
Y=reshape(X,m,n); Z4=Y';
subplot(3,3,5)
imshow(Z4)
title("CB 256 & PSNR = 21.59")

fin=fopen('fingerprintR.raw','r');
X=fread(fin,m*n,'uint8=>uint8'); fclose(fin);
Y=reshape(X,m,n); Z4=Y';
subplot(3,3,6)
imshow(Z4)
title("CB 512 & PSNR = 21.97")


fin=fopen('peppers.raw','r');
X=fread(fin,m*n,'uint8=>uint8'); fclose(fin);
Y=reshape(X,m,n); Z5=Y';
subplot(3,3,7)
imshow(Z5)
title("peppers")

fin=fopen('peppers_256.raw','r');
X=fread(fin,m*n,'uint8=>uint8'); fclose(fin);
Y=reshape(X,m,n); Z6=Y';
subplot(3,3,8)
imshow(Z6)
title("CB 256 & PSNR = 28.96 ")

fin=fopen('peppersR.raw','r');
X=fread(fin,m*n,'uint8=>uint8'); fclose(fin);
Y=reshape(X,m,n); Z6=Y';
subplot(3,3,9)
imshow(Z6)
title("CB 512 & PSNR = 29.79 ")


















