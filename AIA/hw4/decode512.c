/*---------------------------------------------------------------------------*/
/* 2016-4-16 decode512.c VQ decoder by using a codebook of size 512          */
/* a.out                                                                     */
/*---------------------------------------------------------------------------*/
#include <stdio.h>
#define codebooksize 512
#define rows 512
#define cols 512
main()
{FILE *fp, *fout;
 int i,j,k,m,n, index[128][128];
 char str[20];
 unsigned char codebook[codebooksize][4][4];
 unsigned char temp[rows][cols];

/*---------------------------------------------------------------------------*/
/* Input a codebook used for decoding                                        */
/*---------------------------------------------------------------------------*/
  printf("Input the codebook...(size of 512 by 4*4 [codebook512] \n");
  scanf("%s",str);
  fp= fopen(str,"r");
  for(i=0; i<codebooksize;i++)
      for(m=0;m<4;m++)
         for(n=0;n<4;n++)
             codebook[i][m][n]=getc(fp);   
  fclose(fp);
 
/*---------------------------------------------------------------------------*/
/* VQ index for decoding                                                     */
/*---------------------------------------------------------------------------*/
  printf("Input the VQ index file....... [lenna.vq]\n");
  scanf("%s",str);                     
  fp= fopen(str,"r");
  for(i=0;i<128;i++)
      for(j=0;j<128;j++) {
         fscanf(fp,"%d",&k);  index[i][j]=k; 
  }
  fclose(fp);

/*---------------------------------------------------------------------------*/
/* Write to the destination file (a decoded image)                           */
/*---------------------------------------------------------------------------*/
  printf("Write to the destination file (a decoded image)....[lennaR.raw]\n");
  scanf("%s",str);
  fp= fopen(str,"w");

  for(i=0; i<512; i+=4)
    for(j=0; j<512; j+=4) {
      k=index[i>>2][j>>2];
      for(m=0; m<4; m++) 
        for(n=0; n<4; n++)
          temp[i+m][j+n]=codebook[k][m][n];
  }
  for(i=0; i<rows; i++)
    for(j=0; j<cols; j++)
      putc(temp[i][j],fp);
  fclose(fp); 
}/* end of main */

