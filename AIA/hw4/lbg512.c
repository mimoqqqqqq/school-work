/*---------------------------------------------------------------------------*/
/* 105.04.16  lbg512.c - LBG algorithm for a codebook design using 4 images  */
/*  98.07.10  lbg512.c - LBG algorithm for a codebook design using 4 images  */
/*  a.out mandrill.raw tiffany.raw scene.raw lenna.raw  RETURN (waiting)     */
/*        codebook512                                                        */
/*---------------------------------------------------------------------------*/
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#define  N 16384*4       /* Number of training vectors (blocks)  */
#define  K 512           /* Size of codebook                     */
#define  threshold 0.001
#define  iteration 30

main(int argc, char *argv[])
{ 
  int i, count=0;        
  unsigned char vector[N][16];
  unsigned char codebook[K][16];
 
  printf(".....Start Main .....\n");
  for (i=1;i<argc;i++) { 
    printf(" ....%s\n",argv[i]); 
    count= train_vectors(argv[i],vector,count);   
  }
  lbg(vector,codebook);     
}

/*---------------------------------------------------------------------------*/
/* Procedure of training Vectors                                             */
/*---------------------------------------------------------------------------*/
int train_vectors(str,vector,count)
char *str;
int count;
unsigned char vector[N][16];
{ 
  FILE *fp;
  int i,j,k,m;
  unsigned char temp[512][512];
 
  fp= fopen(str,"r");
    for(i=0;i<512;i++)
      for(j=0;j<512;j++)
        temp[i][j]=getc(fp);
  fclose(fp);                 

  for(i=0; i<128; i++)    
    for(j=0; j<128; j++) {
       for(k=0; k<4; k++)
         for(m=0; m<4; m++)
           vector[count][4*k+m]=temp[4*i+k][4*j+m];
       count++;    
  }
  return(count);       
}       
 
/*---------------------------------------------------------------------------*/
/* Procedure of codebook initialization                                      */
/*---------------------------------------------------------------------------*/
codebook_init(vector,codebook)
unsigned char vector[N][16];
unsigned char codebook[K][16];
{ 
  int i,j,k,m, error, init_threshold;
  int val, flag, seed, iran();
  float  ranu(), rann();

/*-------------------------------------------------------------------------***
  printf("..... Input seed and init_threshold .....\n");
  scanf("%d %d",&seed,&init_threshold);
  printf("\n");
---------------------------------------------------------------------------***/

  seed=5731078;   init_threshold=256;
  srandom(seed);  /* srandom(time(NULL));  */
  i=0;
  while(i<K) {
     flag=0;    

     val=iran(0,N-2);
     for(j=0; j<16; j++)    codebook[i][j]=vector[val][j];          
     for(m=0; m<i; m++) { 
       error=0;
       for(j=0; j<16; j++)  error=error+abs(codebook[i][j]-codebook[m][j]);
       if(error<init_threshold) {flag=1; break;}
       else flag=0;        
     } /* end of for m */
     if(flag==1) continue;
     else i++;     
  } /* end of while */   
  printf(".....Initial Codebook is OK;  Waiting for Codebook Generation ...\n");
} /* end of codebook_init */                              
   
/*---------------------------------------------------------------------------*/
/*  Procedure of LBG algorithm                                               */
/*---------------------------------------------------------------------------*/
lbg(vector,codebook)
unsigned char vector[N][16];
unsigned char codebook[K][16];
{ 
  float D[iteration], distortion,sum[K][16];
  int counter[K],i,j,m,n,k,error,dist,bk,count;
  FILE *fp;
  char str[20];

/*---------------------------------------------------------------------------*/
/* Some initial setup parameters                                             */
/*---------------------------------------------------------------------------*/
  D[0]=2e12;
  distortion=2e10;
  k=0;
  codebook_init(vector,codebook);
  for(i=1;i<iteration;i++) D[i]=0;             
/*---------------------------------------------------------------------------*/
/*LBG algorithm                                                              */
/*---------------------------------------------------------------------------*/
  while ((k<iteration) && (distortion>threshold)) {
    k++;     
    for(i=0; i<K; i++) {
      counter[i]=0;  
      for(m=0;m<16;m++)
        sum[i][m]=0;
    } /* end of for i */
                                          
    for(m=0; m<N; m++) {
      error=32767;  
      for(n=0; n<K; n++) {
        dist=0;  
        for(i=0; i<16; i++) {
          dist=dist+(vector[m][i]-codebook[n][i])*(vector[m][i]-codebook[n][i]);
          if (dist>error) break;    
        } 
        if(dist<=error) {error=dist; bk=n;} /* endif */   
      } 
      counter[bk]++;

      D[k]=D[k]+error;
      for(i=0; i<16; i++)
        sum[bk][i]=sum[bk][i]+vector[m][i];
    } /* end of for m */        

    count=0;
    for(n=0; n<K; n++) {
      count=count+counter[n];    
      if(counter[n]!=0) {    
        for(i=0; i<16; i++) {
          codebook[n][i]=sum[n][i]/counter[n];     
        } 
      }
    } 
    distortion= (D[k-1]-D[k])/D[k];
  } /* end of while */  

/*---------------------------------------------------------------------------*/
/* Write to LBG_codebook                                                     */
/*---------------------------------------------------------------------------*/
  printf(" Name a codebook (filename): ");
  scanf("%s",str);
  printf("\n");
  fp=fopen(str,"w");   

  for(i=0; i<K; i++)
    for(j=0;j<16;j++)
      putc(codebook[i][j],fp);
  fclose(fp);
} /* end of lbg */ 


float ranu()        /* uniform distribution:  2147483647 = 2**31-1 */
{  return((float) random() / 2147483647.0);  }

int  iran(a,b)      /* uniform distribution over [a, b] */
int  a, b;
{ return((int) (random()/2147483647.0*(b - a + 1.0) + a)); }

float  rann()   /* standard normal distribution [Knuth, p.114] */
{  float   U1, U2, V1, V2, S;
   label:  U1=random()/2147483647.0;
           U2=random()/2147483647.0;
           V1=U1*2-1.0;
           V2=U2*2-1.0;
           S=V1*V1+V2*V2;
           if (S>=1.0)  goto label;
           return((float) (V1*sqrt(-2.0*log(S)/S)));
}
