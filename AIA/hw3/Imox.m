% lris.m - Linear Discriminant Projection for datairis.txt
%
close all;clear;clc;

fin=fopen('dataimox.txt','r');
d=8+1; N=192;                         % d features, N patterns
fgetl(fin); fgetl(fin); fgetl(fin);  % skip 3 header lines
A=fscanf(fin,'%f',[d N]);  A=A';     % read input data 
X=A(:,1:d-1);                        % remove the label in the last column
k=2;  Y=PCA(X,k);                   % better Matlab code
X1=Y(1:48,1); Y1=Y(1:48,2); 
X2=Y(49:96,1); Y2=Y(49:96,2);
X3=Y(97:144,1); Y3=Y(97:144,2);
X4=Y(145:192,1); Y4=Y(145:192,2);
plot(X1,Y1,'d',X2,Y2,'O',X3,Y3,'X',X4,Y4,'+', 'markersize',10);  
axis([4 24 -2 18]);
legend('I','M','O','X')
title('First Two Principal Component Projection for IMOX Data')

%
% (e) - dendrogram of projected 8OX data set
%
tree = linkage(Y,'complete');

figure()
dendrogram(tree,192)


% (f) - dendrogram of original iris data set
%
tree = linkage(X,'complete');

figure()
dendrogram(tree,192)

