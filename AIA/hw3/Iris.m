% lris.m - Linear Discriminant Projection for datairis.txt
%
close all;clear;clc;
fin=fopen('datairis.txt','r');
d=4+1; N=150;                         % d features, N patterns
fgetl(fin); fgetl(fin); fgetl(fin);  % skip 3 header lines
A=fscanf(fin,'%f',[d N]);  A=A';     % read input data 
X=A(:,1:d-1);                        % remove the label in the last column
k=2;  Y=PCA(X,k);                   % better Matlab code
X1=Y(1:50,1); Y1=Y(1:50,2); 
X2=Y(51:100,1); Y2=Y(51:100,2);
X3=Y(101:150,1); Y3=Y(101:150,2);
plot(X1,Y1,'d',X2,Y2,'O',X3,Y3,'X', 'markersize',4);  
axis([4 24 -2 18]);
legend('setosa','versicolor','virginica')
title('First Two Principal Component Projection for iris Data')

%
% (e) - dendrogram of projected 8OX data set
%
tree = linkage(Y,'complete');

figure()
dendrogram(tree,150)


% (f) - dendrogram of original iris data set
%
tree = linkage(X,'complete');

figure()
dendrogram(tree,150)
