close all;clear;clc;

m=512; n=512;
fin=fopen('lenna.raw','r');
X=fread(fin,m*n,'uint8=>uint8'); fclose(fin);
Y=reshape(X,m,n); Z1=Y';
subplot(2,2,1)
imshow(Z1)
I = double(Z1);
%imshow(I,[]); 

title("mandrill.raw")

% apply sobel filter.
BW_1 = edge(Z1,'sobel',0.15) ;
subplot(2,2,2)
imshow(BW_1);
title("threshold=0.15")

BW_2 = edge(Z1,'sobel',0.1) ;
subplot(2,2,3)
imshow(BW_2);
title("threshold=0.1")

BW_3 = edge(Z1,'sobel',0.05) ;
subplot(2,2,4)
imshow(BW_3);
title("threshold=0.05")