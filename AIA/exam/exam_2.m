close all;clear;clc;
%
% Nijubashi.jpg
%
m=2448; n=3264;
X=imread('Nijubashi.jpg'); 
subplot(2,2,1)
imshow(X)
title('Nijubashi')

R=imhist(X(:,:,1));
G=imhist(X(:,:,2));
B=imhist(X(:,:,3));
subplot(2,2,2)
plot(R,'r')
hold on, plot(G,'g')
plot(B,'b'), legend('R','G','B');
title('Nijubashi RGB histograms')
hold off

%
% Kamakura Daibutsu.jpg
%
m=3448; n=3264;
X_2=imread('Daibutsu.jpg'); 
subplot(2,2,3)
imshow(X_2)
title('Kamakura Daibutsu')

R=imhist(X_2(:,:,1));
G=imhist(X_2(:,:,2));
B=imhist(X_2(:,:,3));
subplot(2,2,4)
plot(R,'r')
hold on, plot(G,'g')
plot(B,'b'), legend('R','G','B');
title('Kamakura Daibutsu RGB histograms')
hold off

close all;clear;clc;
figure
%
% Pumpkin.jpg
%
m=800; n=736;
X_3=imread('Pumpkin.jpg'); 
subplot(2,2,1)
imshow(X_3)
title('Pumpkin')

R=imhist(X_3(:,:,1));
G=imhist(X_3(:,:,2));
B=imhist(X_3(:,:,3));
subplot(2,2,2)
plot(R,'r')
hold on, plot(G,'g')
plot(B,'b'), legend('R','G','B');
title('Pumpkin RGB histograms')
hold off

%
% Carriers.jpg
%

X_4=imread('carriers.jpg'); 
subplot(2,2,3)
imshow(X_4)
title('Carriers')

R=imhist(X_4(:,:,1));
G=imhist(X_4(:,:,2));
B=imhist(X_4(:,:,3));
subplot(2,2,4)
plot(R,'r')
hold on, plot(G,'g')
plot(B,'b'), legend('R','G','B');
title('Carriers RGB histograms')
hold off