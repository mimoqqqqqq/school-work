close all;clear;clc;
% lda8OX2D.m - Linear Discriminant Projection for data8OX.txt
%
fin=fopen('data3d.txt');
nf=3;     n=50;                       % nf features, n patterns
L(1)=25;  L(2)=50;                    % L(3)=n
%fgetl(fin); fgetl(fin); fgetl(fin);   % skip 3 header lines
A=fscanf(fin,'%f',[nf n]); A=A';    % read input data 
d=3;  nk=25;  X=A(:,1:d);
X
%
% (1_b.c.d) - mean vector and Covariance Matrix 
%
X1=X(1:L(1),:);  X2=X(1+L(1):L(2),:); 
u1=mean(X1)
C1=cov(X1)
u2=mean(X2)
C2=cov(X2)
u=mean(X)
C=cov(X)
%
%(1_e) - PCA for XO data set
k=2;  Y_pca=PCA(X,k);                   % better Matlab code
X1_p=Y_pca(1:25,1); Y1_p=Y_pca(1:25,2); 
X2_p=Y_pca(26:50,1); Y2_p=Y_pca(26:50,2);

plot(X1_p,Y1_p,'X',X2_p,Y2_p,'O','markersize',12);  
axis([-4 5 -4 5]);
legend('X','O')
title('First Two Principal Component Projection for 50 Data')

%
% (1_f) - LDA for XO data set
%
W=(nk-1)*(C1+C2);
B=nk*((u1-u)'*(u1-u)+(u2-u)'*(u2-u));
s=0.0001;
C_d=(inv(W+s*eye(d)))*(B+eps)
% - Compute Eigenvalues of W^{-1}B
[U D]=eig(C_d);
Lambda=diag(D);
[Cat index]=sort(Lambda,'descend');
[Cat index]
K=2;
Xproj=zeros(K,d);                     % initiate a projection matrix
for i=1:K
   Xproj(i,:)=U(:,index(i))';
end

Y_d=(Xproj*X')';                        % first K discriminant components
X1_d=Y_d(1:L(1),1); Y1_d=Y_d(1:L(1),2);
X2_d=Y_d(1+L(1):L(2),1); Y2_d=Y_d(1+L(1):L(2),2);
figure;
plot(X1_d,Y1_d,'X',X2_d,Y2_d,'O','markersize',10);  
legend('- X','- O')
axis([-3 3 -3 3]); 
title('First Two Linear Discriminant Projection for 50 data')
%
% (1-g) - dendrogram of projected 8OX data set
%
tree = linkage(X,'average');

figure()
dendrogram(tree,50)

tree = linkage(X,'ward');

figure()
dendrogram(tree,50)