clc;    % Clear the command window.
clear;
close all;  % Close all figures (except those of imtool.)
workspace;  % Make sure the workspace panel is showing.
format long g;
format compact;
fontSize = 22;
id=input('Input picture id:');

if id==2380
grayImage = imread('L2380.jpg');
end
if id==2550
grayImage = imread('L2550.jpg');
end
if id==7176
grayImage = imread('L7176.jpg');
end
if id==7602
grayImage = imread('L7602.jpg');
end


[rows, columns, numberOfColorChannels] = size(grayImage)
if numberOfColorChannels > 1
  grayImage = rgb2gray(grayImage);
end
subplot(2, 3, 1);
imshow(grayImage, []);
axis('on', 'image');
title('Original Image', 'FontSize', fontSize);
% Display histogram of the image, just for fun.
subplot(2, 3, 2);
imhist(grayImage);
grid on;
title('Histogram of Image', 'FontSize', fontSize);
% Binarize the image.
binaryImage = ~imbinarize(grayImage, 0.45);
if id==7602
binaryImage = ~imbinarize(grayImage, 0.3);
end
% Display binary image.
subplot(2, 3, 4);
imshow(binaryImage, []);
axis('on', 'image');
title('Initial Binary Image', 'FontSize', fontSize);
drawnow;
% Get rid of blobs touching the borders except for the left border.
binaryImage(:, 1) = false; % Erase left column so it won't be deleted.
% Delete if touching the top, bottom, or right border.
binaryImage = imclearborder(binaryImage);
% Take the largest of the blobs that remain.
binaryImage = bwareafilt(binaryImage, [1000 3000]);
% Fill the hole.
subplot(2, 3, 5);
imshow(binaryImage, []);
axis('on', 'image');
title('Final Binary Image', 'FontSize', fontSize);
% Find the boundaries.
boundaries = bwboundaries(binaryImage);
% Plot boundaries over the original image.
subplot(2, 3, 6);
imshow(grayImage, []);
hold on
for k=1:length(boundaries)
  boundaryXY=cell2mat(boundaries(k));
  x = boundaryXY(:, 2);
  y = boundaryXY(:, 1);
  plot(x, y, 'r-' , 'LineWidth', 1)
end
title('Image with Boundary ', 'FontSize', fontSize);
axis on
grid on;
